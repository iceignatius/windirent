# dirent for Windows

POSIX dirent functions for Windows platform with UTF-8 encoding.

## Dependency

* utfconv (https://gitlab.com/iceignatius/utfconv)
