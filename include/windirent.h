/**
 * @file
 * @brief POSIX dirent functions for Windows.
 * @details POSIX dirent functions for Windows platform with UTF-8 encoding.
 */
#ifndef _WINDIRENT_H_
#define _WINDIRENT_H_

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief DIR enumeration object.
 */
typedef struct DIR DIR;

/**
 * @brief File type
 */
enum
{
    DT_BLK,         ///< This is a block device.
    DT_CHR,         ///< This is a character device.
    DT_DIR,         ///< This is a directory.
    DT_FIFO,        ///< This is a named pipe (FIFO).
    DT_LNK,         ///< This is a symbolic link.
    DT_REG,         ///< This is a regular file.
    DT_SOCK,        ///< This is a UNIX domain socket.
    DT_UNKNOWN,     ///< The file type could not be determined.
};

/**
 * @brief Directory entry information.
 */
struct dirent
{
#define _DIRENT_HAVE_D_RECLEN
#define _DIRENT_HAVE_D_TYPE
    unsigned        d_ino;          ///< Not used!
    unsigned short  d_reclen;       ///< Length of this record.
    unsigned char   d_type;         ///< File type; not supported by all file system types.
    char            d_name[1024];   ///< Null-terminated file name.
};

DIR* windirent_opendir(const char *name);
int windirent_closedir(DIR *dir);

struct dirent* windirent_readdir(DIR *dir);

// The following functions mapping are designed to
// prevent the conflict with MinGW implementation.
#define opendir     windirent_opendir
#define closedir    windirent_closedir
#define readdir     windirent_readdir
#define rewinddir   // Not implemented yet!
#define scandir     // Not implemented yet!
#define seekdir     // Not implemented yet!
#define telldir     // Not implemented yet!

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
